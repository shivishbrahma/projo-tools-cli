from urllib.parse import urlparse
import requests
from bs4 import BeautifulSoup
from argparse import ArgumentParser
from markdownify import markdownify, MarkdownConverter
import sys
import base64
import mimetypes
from datauri import DataURI


# Create new image block converter
class ImageBlockConverter(MarkdownConverter):
    """
    Create a custom MarkdownConverter that adds two newlines after an image
    """
    def convert_img(self, el, text, convert_as_inline):
        alt = el.attrs.get('alt', None) or ''
        src = el.attrs.get('src', None) or ''
        if src.startswith('http') or src.startswith('https'):
            src = pull_image(src)
        title = el.attrs.get('title', None) or ''
        title_part = ' "%s"' % title.replace('"', r'\"') if title else ''
        if (convert_as_inline and el.parent.name
                not in self.options['keep_inline_images_in']):
            return alt

        return '![%s%s](%s)' % (alt, title_part, src)


# Create shorthand method for conversion
def html2md(html, **options):
    return ImageBlockConverter(**options).convert(html)


# Pull image from URL and convert to Data URI
def pull_image(url):
    response = requests.get(url)
    mime, _ = mimetypes.guess_type(url)
    if response.status_code == 200:
        data64 = base64.b64encode(response.content).decode('utf-8')
        return u'data:%s;base64,%s' % (mime, data64)
    else:
        print('Error: {}'.format(response.status_code))
        sys.exit(1)


# Pull the post from the URL
def pull_post(url: str, selector: str):
    print("Pulling post from: " + url)
    response = requests.get(url)
    if response.status_code == 200:
        soup = BeautifulSoup(response.text, 'html.parser')
        post = soup.select_one(selector)
        print("Post pulled")
        return str(post)
    else:
        print('Error: {}'.format(response.status_code))
        sys.exit(1)


def main():
    parser = ArgumentParser()
    parser.add_argument('-u',
                        '--url',
                        help='Enter the post URL',
                        required=True)
    parser.add_argument('-s',
                        '--selector',
                        help='Enter the post selector',
                        required=True)
    parser.add_argument('-o',
                        '--output',
                        help='Enter the output file',
                        required=True)
    args = parser.parse_args()

    if args.url:
        url = args.url
    if args.selector:
        selector = args.selector
    if args.output:
        output = args.output
    if output is None:
        urlparse(url)

    post = pull_post(url, selector)
    with open(output, 'w') as f:
        f.write(html2md(post))
        print("Post written to: " + output)


if __name__ == '__main__':
    # Default parameters
    url = ""
    selector = "article"
    output = None

    main()