from tkinter import Tk, StringVar, IntVar, Label, Text, Button, Entry, END, messagebox, ttk, font
import tkinter as tk
import json
import os
from text_with_var import TextWithVar
from dotenv import load_dotenv


def alert(title, message, kind='info', hidemain=True):
    if kind not in ('error', 'warning', 'info'):
        raise ValueError('Unsupported alert kind.')

    show_method = getattr(messagebox, 'show{}'.format(kind))
    show_method(title, message)


def insert_quote():
    quote_text = add_quote_text_var.get()
    quote_tags = add_quote_tags_var.get()
    quote_author = add_quote_author_var.get()
    if quote_text == "":
        return False
    if quote_tags == "":
        quote_tags = []
    else:
        quote_tags = quote_tags.split(", ")
    # print(quote_text, quote_tags, quote_author, sep="\n")
    try:
        save_to_json_file(quotes_file_name, {
            "text": quote_text,
            "tags": quote_tags,
            "author": quote_author
        })
        alert("Success", "Quote saved successfully.", kind='info')
        add_quote_text_field.delete("1.0", END)
        add_quote_tags_var.set("")
        add_quote_author_var.set("")
    except Exception as e:
        alert("Error", "Error saving quote.\n{}".format(e), kind='error')


def update_quote():
    quote_text = view_quote_text_var.get()
    quote_tags = view_quote_tags_var.get()
    quote_author = view_quote_author_var.get()
    if quote_text == "":
        return False
    if quote_tags == "":
        quote_tags = []
    else:
        quote_tags = quote_tags.split(", ")
    # print(quote_text, quote_tags, quote_author, sep="\n")
    try:
        save_to_json_file(quotes_file_name, {
            "text": quote_text,
            "tags": quote_tags,
            "author": quote_author
        }, view_quote_index.get())
        alert("Success", "Quote updated successfully.", kind='info')
    except Exception as e:
        alert("Error", "Error updating quote.\n{}".format(e), kind='error')


def get_quote(index: int):
    quotes = get_from_json_file(quotes_file_name)
    if index < len(quotes):
        return quotes[index]
    return {}


def save_to_json_file(path: str, data: dict, index: int = None):
    if os.path.exists(path):
        quotes = get_from_json_file(path)
        if index is not None:
            quotes[index] = data
        elif data is not None:
            quotes.append(data)
    else:
        quotes = [data]
    with open(path, "w") as file:
        json.dump(quotes, file, indent=4)


def get_from_json_file(path: str) -> list:
    if os.path.exists(path):
        with open(path, "r") as f:
            quotes = json.load(f)
        return quotes
    return {}


def next_quote():
    index = view_quote_index.get() + 1
    index = 0 if index >= len(get_from_json_file(quotes_file_name)) else index
    view_quote_index.set(index)
    quote = get_quote(index)
    view_quote_text_var.set(quote.get("text", ""))
    view_quote_tags_var.set(",".join(quote.get("tags", [])))
    view_quote_author_var.set(quote.get("author", ""))


def prev_quote():
    index = view_quote_index.get() - 1
    index = len(
        get_from_json_file(quotes_file_name)) - 1 if index < 0 else index
    view_quote_index.set(index)
    quote = get_quote(index)
    view_quote_text_var.set(quote.get("text", ""))
    view_quote_tags_var.set(",".join(quote.get("tags", [])))
    view_quote_author_var.set(quote.get("author", ""))


if __name__ == '__main__':
    window = Tk()

    font_tuple = {
        "label": ("Open Sans", 12),
        "button": ("DejaVu Sans Mono", 10),
        "field": ("Comic Sans MS", 12),
    }

    __dir__ = os.path.dirname(os.path.realpath(__file__))

    load_dotenv(os.path.join(__dir__, 'config.txt'))

    quotes_file_name = "quotes.json"
    quotes_file_name = os.path.join(__dir__, os.getenv("QUOTES_FILE"))

    logo_icon_filename = os.path.join(__dir__, os.getenv("LOGO_FILE"))

    window.geometry("800x800")
    window.title("Quotex")

    logo_icon = tk.PhotoImage(file=logo_icon_filename)
    window.iconphoto(True, logo_icon)

    tab_control = ttk.Notebook(window)

    add_quote_tab = ttk.Frame(tab_control)
    view_quote_tab = ttk.Frame(tab_control)

    tab_control.add(add_quote_tab, text="Add")
    tab_control.add(view_quote_tab, text="View")
    tab_control.pack(expand=1, fill="both")

    # Variables
    add_quote_text_var = StringVar()
    add_quote_tags_var = StringVar()
    add_quote_author_var = StringVar()

    view_quote_index = IntVar(value=0)

    quote = get_quote(view_quote_index.get())
    view_quote_text_var = StringVar(value=quote.get("text", ""))
    view_quote_tags_var = StringVar(value=quote.get("tags", ""))
    view_quote_author_var = StringVar(value=quote.get("author", ""))

    # Add Quote Tab
    add_quote_text_label = Label(add_quote_tab, text="Quote:", width=20)
    add_quote_text_field = Text(add_quote_tab, width=50, height=10)
    add_quote_tags_label = Label(add_quote_tab, text="Tags:", width=20)
    add_quote_tags_field = Entry(add_quote_tab,
                                 textvariable=add_quote_tags_var,
                                 width=50)
    add_quote_author_label = Label(add_quote_tab, text="Author:", width=20)
    add_quote_author_field = Entry(add_quote_tab,
                                   textvariable=add_quote_author_var,
                                   width=50)
    add_quote_submit_btn = Button(add_quote_tab,
                                  text="Submit",
                                  command=insert_quote)
    add_quote_close_btn = Button(add_quote_tab,
                                 text="Close",
                                 command=lambda: window.destroy())

    add_quote_text_field.bind(
        "<KeyRelease>", lambda event: add_quote_text_var.set(
            add_quote_text_field.get("1.0", END)))

    add_quote_text_label.grid(row=0,
                              column=0,
                              padx=10,
                              pady=10,
                              ipadx=3,
                              ipady=4)
    add_quote_text_field.grid(row=0,
                              column=1,
                              padx=10,
                              pady=10,
                              ipadx=3,
                              ipady=4)
    add_quote_tags_label.grid(row=1,
                              column=0,
                              padx=10,
                              pady=10,
                              ipadx=3,
                              ipady=4)
    add_quote_tags_field.grid(row=1,
                              column=1,
                              padx=10,
                              pady=10,
                              ipadx=3,
                              ipady=4)
    add_quote_author_label.grid(row=2,
                                column=0,
                                padx=10,
                                pady=10,
                                ipadx=3,
                                ipady=4)
    add_quote_author_field.grid(row=2,
                                column=1,
                                padx=10,
                                pady=10,
                                ipadx=3,
                                ipady=4)

    add_quote_submit_btn.place(x=400, y=350)
    add_quote_close_btn.place(x=250, y=350)

    add_quote_text_label.configure(font=font_tuple["label"])
    add_quote_text_field.configure(font=font_tuple["field"])
    add_quote_tags_label.configure(font=font_tuple["label"])
    add_quote_tags_field.configure(font=font_tuple["field"])
    add_quote_author_label.configure(font=font_tuple["label"])
    add_quote_author_field.configure(font=font_tuple["field"])

    add_quote_submit_btn.configure(font=font_tuple["button"])
    add_quote_close_btn.configure(font=font_tuple["button"])

    # View Quote Tab
    view_quote_index_label = Label(view_quote_tab,
                                   textvariable=view_quote_index,
                                   width=20)
    view_quote_text_label = Label(view_quote_tab, text="Quote:", width=20)
    view_quote_text_field = TextWithVar(view_quote_tab,
                                        width=50,
                                        height=10,
                                        textvariable=view_quote_text_var)
    view_quote_tags_label = Label(view_quote_tab, text="Tags:", width=20)

    view_quote_tags_field = Entry(view_quote_tab,
                                  textvariable=view_quote_tags_var,
                                  width=50)

    view_quote_author_label = Label(view_quote_tab, text="Author:", width=20)
    view_quote_author_field = Entry(view_quote_tab,
                                    textvariable=view_quote_author_var,
                                    width=50)

    view_quote_prev_btn = Button(view_quote_tab,
                                 text="<= Prev",
                                 command=prev_quote)
    view_quote_next_btn = Button(view_quote_tab,
                                 text="Next =>",
                                 command=next_quote)
    view_quote_update_btn = Button(view_quote_tab,
                                   text="Update",
                                   command=update_quote)

    view_quote_index_label.grid(row=0,
                                column=0,
                                padx=10,
                                pady=10,
                                ipadx=3,
                                ipady=4)
    view_quote_text_label.grid(row=1,
                               column=0,
                               padx=10,
                               pady=10,
                               ipadx=3,
                               ipady=4)
    view_quote_text_field.grid(row=1,
                               column=1,
                               padx=10,
                               pady=10,
                               ipadx=3,
                               ipady=4)
    view_quote_tags_label.grid(row=2,
                               column=0,
                               padx=10,
                               pady=10,
                               ipadx=3,
                               ipady=4)
    view_quote_tags_field.grid(row=2,
                               column=1,
                               padx=10,
                               pady=10,
                               ipadx=3,
                               ipady=4)
    view_quote_author_label.grid(row=3,
                                 column=0,
                                 padx=10,
                                 pady=10,
                                 ipadx=3,
                                 ipady=4)
    view_quote_author_field.grid(row=3,
                                 column=1,
                                 padx=10,
                                 pady=10,
                                 ipadx=3,
                                 ipady=4)

    view_quote_prev_btn.grid(row=5, column=0, padx=10, pady=40)
    view_quote_update_btn.grid(row=5, column=1, padx=10, pady=40)
    view_quote_next_btn.grid(row=5, column=2, padx=10, pady=40, sticky='E')

    view_quote_index_label.configure(font=font_tuple["label"])
    view_quote_text_label.configure(font=font_tuple["label"])
    view_quote_text_field.configure(font=font_tuple["field"])
    view_quote_tags_label.configure(font=font_tuple["label"])
    view_quote_tags_field.configure(font=font_tuple["field"])
    view_quote_author_label.configure(font=font_tuple["label"])
    view_quote_author_field.configure(font=font_tuple["field"])

    view_quote_prev_btn.configure(font=font_tuple["button"])
    view_quote_next_btn.configure(font=font_tuple["button"])
    view_quote_update_btn.configure(font=font_tuple["button"])

    window.mainloop()
